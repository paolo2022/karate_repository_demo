function fn() {    
 //var env = karate.env; // get system property 'karate.env'
 karate.configure('logPrettyResponse', true)
  var env = java.lang.System.getenv('karateEnv');
  karate.log('karate.env system property was:', env);
  if (!env) {
    env = 'qa';
  }
  var config = {
    env: env,
	myVarName: 'someValue'
  }
  if (env == 'local') {
   config.url= "https://localhost"
  } else if (env == 'qa') {
    config.url= "https://reqres.in/api"
  }
  return config;
}
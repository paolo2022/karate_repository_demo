Feature: Registrar y loguear a usuario
  Background:
    * url url
    #* header Accept = 'application/json'
    #* header Content-Type = 'application/json'

    Scenario: Registrar usuario
      * def body =
      """
      {
    "email": "eve.holt@reqres.in",
    "password": "pistol"
}
      """
      Given path 'register'
      And request body
      When method post
      Then status 200


Scenario: loguear a un usuario
  * def body =
      """
      {
    "email": "eve.holt@reqres.in",
    "password": "cityslicka"
      }
      """
  Given path 'login'
  And request body
  When method post
  Then status 200


  Scenario: Loguear a un usuario con json
    * def data = read('classpath:src/test/java/examples/registrousuario/registrousuario.json')
    * def body =
      """
      {
    "email": "#(data.email)",
    "password": "#(data.password)"
      }
      """
    Given path 'login'
    And request body
    When method post
    Then status 200


  Scenario: Loguear a un usuario no ingrese password
    * def body =
      """
      {
    "email": "peter@klaven",

}
      """
    Given path 'login'
    And request body
    When method post
    Then status 400
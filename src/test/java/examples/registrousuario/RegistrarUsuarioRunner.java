package examples.registrousuario;

import com.intuit.karate.junit5.Karate;

public class RegistrarUsuarioRunner {
    @Karate.Test
    Karate testUsers() {
        return Karate.run("registrarusuario").relativeTo(getClass());}
}

package examples.crearusuarios;

import com.intuit.karate.junit5.Karate;

public class CreateUsuariosRunner {
    @Karate.Test
    Karate testUsers() {
        return Karate.run("crearusuarios").relativeTo(getClass());}

}

Feature: Apis usuarios

  Background:
    * url url
    * header Accept = 'application/json'
    * header Content-Type = 'application/json'
    #* def id= id

  Scenario: Crear usuario
    * def body =
    """
    {
    "name": "paolo",
    "job": "criales",
    }
    """
    Given path 'users'
    And request body
    When  method post
    Then status 201
    And match response.name == 'paolo'

  Scenario: Actualizar usuario
    * def body =
    """
    {
    "name": "steve",
    "job": "salinas",
    "id": "654",
    "createdAt": "#ignore"

}
    """
    Given path 'users'
    And request body
    When  method put
    Then status 200
    And match response.name == 'steve'

  Scenario: Actualizar usuario con patch
    * def body =
    """
    {
    "name": "jose",
    "job": "salinas"
}
    """
    Given path 'users'
    And request body
    When  method patch
    Then status 200
    And match response.name == 'jose'

  Scenario: crear usuario with json
    * def data = read('classpath:src/test/java/examples/crearusuarios/data.json')
    * def body =
    """
    {
    "name": "#(data.name)",
    "job": "#(data.job)",
    "id": "#(data.id)",
    "createdAt": "#(createdAt)"

    }
    """
    Given path 'users'
    And request body
    When method post
    Then  status 201
    And match response.name contains data.name
    And match response.job contains data.job
    And match response.id contains data.id
    And match response.createdAt contains data.createdAt

  Scenario Outline: crear usuario with datatable <name>
    * def body =
    """
    {
    "name": "<name>",
    "job": "<job>",
    "id": "<id>",

    }
    """
    Given path 'users'
    And request body
    When method post
    Then  status 201
    And match response.name contains '<name>'
    And match response.job contains '<job>'
    And match response.id contains '<id>'
    Examples:
      | name  | job | id  |
      | paolo | QA  | 456 |

  Scenario Outline: crear usuario with json2 <name>

    * def body =
    """
    {
    "name": "#(name)",
    "job": "#(job)",
    "id": "#(id)",
    "createdAt": "#(createdAt)"

    }
    """
    Given path 'users'
    And request body
    When method post
    Then  status 201
    And match response.name contains name
    And match response.job contains job
    And match response.id contains id
    And match response.createdAt contains createdAt
    Examples:
    |read('datos.json')|
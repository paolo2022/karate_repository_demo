Feature: API Usuarios
 Background: 
   * url url

  Scenario: Get all users

    Given path 'users'
    When  method get
    Then status 200
    * def count = response.length
    * print count
    

  Scenario: Get  users details <id>
    * def id = 1
    Given path 'users', id
    When  method get
    Then status 200
    And match response.data.email == 'george.bluth@reqres.in'
    And match response.data.first_name == 'George'
    And match response.data.last_name == 'Bluth'
    And match response.support.url == 'https://reqres.in/#support-heading'
    And match response.support.text == 'To keep ReqRes free, contributions towards server costs are appreciated!'

  Scenario:  Get users datails no found
    * def id = 23
    Given path 'users', id
    When  method get
    Then status 404
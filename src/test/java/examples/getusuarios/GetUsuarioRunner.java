package examples.getusuarios;

import com.intuit.karate.junit5.Karate;

public class GetUsuarioRunner {
    @Karate.Test
    Karate testUsers() {
        return Karate.run("getusuarios").relativeTo(getClass());
    }
}
